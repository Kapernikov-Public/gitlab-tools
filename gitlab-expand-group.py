#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""Handy script for adding group members to repos directly.

Built on top of GitLab Python API: https://python-gitlab.readthedocs.io/

Details: Imaging you have a group that has an access to repos.
The group has members and as a result they have indirect access to same repos.
Using this script you can add group members directly to repos.
"""

import argparse

import gitlab


def parse_args():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Expand gitlab group by adding users to repos directly.')
    parser.add_argument('--group', required=True,
                        help='Gitlab group you want to expand')
    parser.add_argument('--token', required=True,
                        help='Gitlab token of your user')
    parser.add_argument('--url', default='https://gitlab.com',
                        help='Gitlab URL')
    parser.add_argument('--visibility', default='private',
                        help='Gitlab projects visibility')
    parser.add_argument('--dry', action='store_true',
                        help='dry run')
    return parser.parse_args()


def print_ok():
    """Print fancy ok message."""
    print('🟢 ok')


def print_fail():
    """Print fancy fail message."""
    print('🔴 fail')


def print_skip():
    """Print fancy skip message."""
    print('🟡 skip')


def main():
    """Script entry point."""
    # Greeting and args parsing.
    args = parse_args()

    # Initialize Gitlab API.
    print(f'💬 Auth to {args.url} : ', end='')
    gl = gitlab.Gitlab(args.url, private_token=args.token)
    try:
        gl.auth()
        print_ok()
    except:  # noqa
        print_fail()
        return

    print(f'Searching for group "{args.group}" ', end='')
    try:
        g = gl.groups.get(args.group)
        print_ok()
    except gitlab.GitlabGetError:
        print_fail()
        return

    for gp in g.projects.list(all=True):
        print(f'\nProject "{gp.name_with_namespace}" (id={gp.id}) :')
        p = gl.projects.get(gp.id)
        group_levels = [i['group_access_level'] for i in p.shared_with_groups
                        if i['group_name'] == args.group]
        assert len(group_levels) < 2
        group_level = 100 if len(group_levels) == 0 else group_levels[0]
        add_members_count = 0
        for m in g.members.list(all=True):
            al = min(group_level, m.access_level)
            print(f'  Add "{m.name}" (id={m.id}) with access level {al} : ', end='')  # noqa
            if not args.dry:
                try:
                    p.members.create({'user_id': m.id, 'access_level': al})
                    print_ok()
                    add_members_count += 1
                except:  # noqa
                    print_fail()
            else:
                print_skip()
        print(f'  💬 {add_members_count} members are added')

    return


if __name__ == '__main__':
    main()
