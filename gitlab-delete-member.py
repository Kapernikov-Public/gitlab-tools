#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Script for searching and deleting members from Gitlab projects.
Built on top of GitLab Python API: https://python-gitlab.readthedocs.io/
"""

import argparse
from collections import namedtuple

import gitlab


def parse_args():
    parser = argparse.ArgumentParser(
        description='Delete member from all gitlab groups and projects '
                    'descending from a given group.')
    parser.add_argument('--token', required=True,
                        help='Gitlab API personal access token for your user')
    parser.add_argument('--parent-group', required=True,
                        help='user is only deleted from this group and all '
                             'descendant subgroups and projects')
    parser.add_argument('--username', required=True,
                        help='Gitlab username of the user to be deleted')
    parser.add_argument('--url', default='https://gitlab.com',
                        help='Gitlab URL')
    parser.add_argument('--delete', action='store_true',
                        help='delete the user from the groups and projects. '
                             'Without this flag, the groups and projects are '
                             'just listed.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='print out all groups and projects, even those '
                             'the user does not belong to')
    return parser.parse_args()


def print_ok():
    print('🟢 ok')


def print_fail():
    print('🔴 fail')


def main():
    # Greeting and args parsing.
    args = parse_args()

    # Initialize Gitlab API.
    print(f'Auth to {args.url} with the given Personal Access Token: ', end='')
    gl = gitlab.Gitlab(args.url, private_token=args.token)
    try:
        gl.auth()
    except:  # noqa
        print_fail()
        return
    else:
        print_ok()

    # Find the parent group
    parent_group = gl.groups.get(args.parent_group)

    # Find the user to delete
    user = gl.users.list(username=args.username)[0]

    total_projects_count = 0

    GroupMembership = namedtuple('GroupMembership', 'group, member')
    groups_to_delete_from = []
    ProjectMembership = namedtuple('ProjectMembership', 'project, member')
    projects_to_delete_from = []

    print()
    print(f'Membership of user "{user.username}" ({user.name}) for all groups '
          f'and projects descending from group "{parent_group.full_name}":')

    # Iterate over groups
    all_groups = [parent_group]
    all_groups.extend(sorted(parent_group.descendant_groups.list(get_all=True),
                             key=lambda g: g.full_name))

    for group in all_groups:
        group_lazy = gl.groups.get(group.id, lazy=True)
        group_printed = False

        try:
            m = group_lazy.members.get(user.id)
        except gitlab.GitlabGetError as err:
            if args.verbose:
                print(f'  Group {group.full_name}')
                group_printed = True
            if err.error_message != '404 Not found':
                raise
        else:
            groups_to_delete_from.append(GroupMembership(group, m))
            print(f'  Group {group.full_name}: 🟡 user is member')
            group_printed = True

        # Iterate over projects in this group
        for project in sorted(group_lazy.projects.list(get_all=True),
                              key=lambda p: p.name_with_namespace):
            total_projects_count += 1
            project_lazy = gl.projects.get(project.id, lazy=True)

            try:
                m = project_lazy.members.get(user.id)
            except gitlab.GitlabGetError as err:
                if err.error_message != '404 Not found':
                    raise
                if args.verbose:
                    print(f'    Project {project.name_with_namespace}')
            else:
                projects_to_delete_from.append(ProjectMembership(project, m))
                if not group_printed:
                    print(f'  Group {group.full_name}')
                    group_printed = True
                print(f'    Project {project.name_with_namespace}: '
                      '🟡 user is member')

    # Summary stats
    print()
    print(f'User found in {len(groups_to_delete_from)} '
          f'out of {len(all_groups)} groups')
    print(f'User found in {len(projects_to_delete_from)} '
          f'out of {total_projects_count} projects')

    if args.delete and (groups_to_delete_from or projects_to_delete_from):
        # Ask confirmation
        print()
        print(f'Do you really want to delete {user.name} from all above '
              'GitLab groups and projects?\n'
              'Type their GitLab username to confirm and press ENTER.')
        answer = input('--> Username: ')
        print()
        if answer == user.username:
            # Delete from groups
            if groups_to_delete_from:
                print('Deleting user from groups...')
                for gm in groups_to_delete_from:
                    print(f'  {gm.group.full_name}: ', end='')
                    try:
                        gm.member.delete()
                    except gitlab.GitlabDeleteError as err:
                        print('🔴 failure')
                        if err.error_message != '404 Not found':
                            raise
                    else:
                        print('🟢 done')
            else:
                print('No groups to delete the user from.')

            # Delete from projects
            if projects_to_delete_from:
                print('Deleting user from projects...')
                for pm in projects_to_delete_from:
                    print(f'  Project {pm.project.name_with_namespace}: ',
                          end='')
                    try:
                        pm.member.delete()
                    except gitlab.GitlabDeleteError as err:
                        print('🔴 failure')
                        if err.error_message != '404 Not found':
                            raise
                    else:
                        print('🟢 done')
            else:
                print('No projects to delete the user from.')
        else:
            print('Wrong username. Aborted.')


if __name__ == '__main__':
    main()
