#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""Script for listing all members of a given group and all of its descendant
groups and projects.
Built on top of GitLab Python API: https://python-gitlab.readthedocs.io/
"""

import argparse
from dataclasses import dataclass

import gitlab

import progressbar


# From https://docs.gitlab.com/ee/api/access_requests.html#valid-access-levels
GITLAB_ACCESS_LEVELS = {
    0: "No access",
    5: "Minimal access",
    10: "Guest",
    20: "Reporter",
    30: "Developer",
    40: "Maintainer",
    50: "Owner"
}


def parse_args():
    parser = argparse.ArgumentParser(
        description='List all GitLab users that are member of any subgroup or '
                    'project descending from a given group.')
    parser.add_argument('--token', required=True,
                        help='Gitlab API personal access token for your user')
    parser.add_argument('--parent-group', required=True,
                        help='parent group to search in')
    parser.add_argument('--url', default='https://gitlab.com',
                        help='Gitlab URL')
    return parser.parse_args()


def print_ok():
    print('🟢 ok')


def print_fail():
    print('🔴 fail')


@dataclass(order=True, frozen=True)
class Member:
    username: str
    name: str
    id: int


class Membership:
    def __init__(self):
        self.groups = []
        self.projects = []

    def __repr__(self):
        groups = [g.full_path for g, m in self.groups]
        projects = [p.path_with_namespace for p, m in self.projects]
        return (f'Membership(groups={groups}, projects={projects})')


def main():
    # Greeting and args parsing.
    args = parse_args()

    # Initialize Gitlab API.
    print(f'Auth to {args.url} with the given Personal Access Token: ', end='')
    gl = gitlab.Gitlab(args.url, private_token=args.token)
    try:
        gl.auth()
    except:  # noqa
        print_fail()
        return
    else:
        print_ok()

    # Find the parent group
    print('Getting groups... ', end='')
    parent_group = gl.groups.get(args.parent_group)
    descendent_groups = parent_group.descendant_groups.list(get_all=True)
    print('Done.')

    print('Getting projects... ', end='')
    descendent_projects = parent_group.projects.list(include_subgroups=True,
                                                     get_all=True)
    print('Done.')

    members = {}

    count = 0
    all_groups = [parent_group] + descendent_groups
    for g in all_groups:
        count += 1
        progressbar.progress(count, len(all_groups),
                             title='Getting group members')
        lazy_group = gl.groups.get(g.id, lazy=True)
        for m in lazy_group.members.list(iterator=True):
            mm = Member(m.username, m.name, m.id)
            if mm not in members:
                members[mm] = Membership()
            members[mm].groups.append((g, m))
    print()

    count = 0
    if descendent_projects:
        for p in descendent_projects:
            count += 1
            progressbar.progress(count, len(descendent_projects),
                                 title='Getting project members')
            lazy_project = gl.projects.get(p.id, lazy=True)
            for m in lazy_project.members.list(iterator=True):
                mm = Member(m.username, m.name, m.id)
                if mm not in members:
                    members[mm] = Membership()
                members[mm].projects.append((p, m))
        print()
    else:
        print('No projects')

    # Write a list of the members,
    # together with the groups and projects they have access to
    print()
    for m in sorted(members, key=lambda m: m.name.casefold()):
        def group_sort_key(g):
            return g[0].full_name.casefold()

        def project_sort_key(p):
            return p[0].name_with_namespace.casefold()

        print(f"{m.name} ({m.username}):")
        for g, gm in sorted(members[m].groups, key=group_sort_key):
            access_level = GITLAB_ACCESS_LEVELS.get(gm.access_level,
                                                    '(unknown access level)')
            print(f"  Group {g.full_name} ({access_level})")
        for p, pm in sorted(members[m].projects, key=project_sort_key):
            access_level = GITLAB_ACCESS_LEVELS.get(pm.access_level,
                                                    '(unknown access level)')
            print(f"  Project {p.name_with_namespace} ({access_level})")


if __name__ == '__main__':
    main()
