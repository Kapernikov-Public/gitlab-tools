# Adapted from https://gist.github.com/vladignatyev/06860ec2040cb497f0f3

# The MIT License (MIT)
# Copyright (c) 2016 Vladimir Ignatev
# Copyright (c) 2022 Jan Frederik Léger
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys


def progress(count, total, status='', title='', bar_length=60):
    filled_len = int(round(bar_length * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_length - filled_len)

    if title:
        title = f'{title}: '
    if status:
        status = f' ...{status}'
    sys.stdout.write(f'{title}[{bar}] {percents}%{status}\r')
    # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)  # noqa
    sys.stdout.flush()


def clear(status='', title='', bar_length=60):
    if title:
        title = f'{title}: '
    if status:
        status = f' ...{status}'
    spaces = ' ' * (len(title) + 2 + bar_length + 7 + len(status))
    sys.stdout.write(f'{spaces}\r')
    # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)  # noqa
    sys.stdout.flush()
