# gitlab-tools

Handy scripts for automating typical operations in Gitlab.
Built on top of GitLab Python API: https://python-gitlab.readthedocs.io/

You need a GitLab personal access token with API access.
You can create one here: https://gitlab.com/-/profile/personal_access_tokens.


## Build and use as docker container
Build:

```
$ docker build --tag gitlab-tools .
```

Run: (note that you should not specify the `.py` extension for the script!)
```
$ docker run --rm -it gitlab-tools the_script_you_want_to_run arg1 arg2 arg3
```



## Install and run with Poetry
Make sure you have Python >= 3.8 somewhere on your system.

[Install poetry](https://python-poetry.org/docs/master/#installing-with-the-official-installer).

Create new poetry environment with all dependencies:
```
$ poetry install --no-dev
```

Run the scripts:
```
$ poetry run ./the_script_you_want_to_run.py arg1 arg2 arg3
```


## gitlab-list-members.py

### Usage:

```
gitlab-list-members.py [-h] --token TOKEN --parent-group PARENT_GROUP [--url URL]

List all GitLab users that are direct member of any subgroup or project descending from a given group.

required arguments:
  --token TOKEN         Gitlab API personal access token for your user
  --parent-group PARENT_GROUP
                        parent group to search in

optional arguments:
  -h, --help            show this help message and exit
  --url URL             Gitlab URL
```


### Example

You gitlab token is `glpat-Abcdefg123`. You want to list all users that are defined as direct member anywhere under
the group "MyGitlabGroup".

```
$ gitlab-list-members.py --token glpat-Abcdefg123 --parent-group MyGitlabGroup
Auth to https://gitlab.com with the given Personal Access Token: 🟢 ok
Getting groups... Done.
Getting projects... Done.
Getting group members: [============================================================] 100.0%
Getting project members: [============================================================] 100.0%

John Doe (joehndoe123):
  Group MyGitlabGroup / SomeSubGroup (Owner)
Jane Doe (jane.doe.xyz):
  Group MyGitlabGroup (Reporter)
  Group MyGitlabGroup / AnotherSubGroup (Developer)
  Project MyGitlabGroup / A First project (Maintainer)
  Project MyGitlabGroup / AnotherSubGroup / My Spam Project (Owner)
  Project MyGitlabGroup / SomeSubGroup / MyProjectBar (Owner)
```


## gitlab-delete-member.py

### Usage:

```
gitlab-delete-member.py [-h] --token TOKEN --parent-group PARENT_GROUP --username USERNAME
                               [--url URL] [--delete] [-v]

Delete direct member from all gitlab groups and projects descending from a given group.

required arguments:
  --token TOKEN         Gitlab API personal access token for your user
  --parent-group PARENT_GROUP
                        user is only deleted from this group and all descendant subgroups and
                        projects
  --username USERNAME   Gitlab username of the user to be deleted

optional arguments:
  -h, --help            show this help message and exit
  --url URL             Gitlab URL
  --delete              delete the user from the groups and projects. Without this flag, the
                        groups and projects are just listed.
  -v, --verbose         print out all groups and projects, even those the user does not belong to
```


### Example

You gitlab token is `glpat-Abcdefg123`. You want to delete user with username `johndoe123` from all accessible projects
under the group "MyGitlabGroup".
Even with the flag `--delete`, you are first presented with the list of groups and projects the user would be deleted
from and you are required to confirm explicitly your intention to delete.

```
$ gitlab-delete-member.py --token glpat-Abcdefg123 --parent-group MyGitlabGroup --username johndoe123 --delete -v
Auth to https://gitlab.com with the given Personal Access Token: 🟢 ok

Membership of user "johndoe123" (John Doe) for all groups and projects descending from group "MyGitlabGroup":
  Group MyGitlabGroup
    Project MyGitlabGroup / A First project
  Group MyGitlabGroup / AnotherSubGroup: 🟡 user is member
    Project MyGitlabGroup / AnotherSubGroup / My Spam Project
    Project MyGitlabGroup / AnotherSubGroup / My Eggs Project
  Group MyGitlabGroup / SomeSubGroup
    Project MyGitlabGroup / SomeSubGroup / MyProjectFoo: 🟡 user is member
    Project MyGitlabGroup / SomeSubGroup / MyProjectBar: 🟡 user is member

User found in 1 out of 3 groups
User found in 2 out of 4 projects

Do you really want to delete John Doe from all above GitLab groups and projects?
Type their GitLab username to confirm and press ENTER.
--> Username: johndoe123

Deleting user from groups...
  MyGitlabGroup / AnotherSubGroup: 🟢 done
Deleting user from projects...
  MyGitlabGroup / SomeSubGroup / MyProjectFoo: 🟢 done
  MyGitlabGroup / SomeSubGroup / MyProjectBar: 🟢 done
```
