FROM python:3.8-slim-buster
MAINTAINER janfrederik@kapernikov

RUN apt-get update \
    && apt-get install --no-install-recommends --yes curl \
    && rm -rf /var/lib/apt/lists/

ENV POETRY_VERSION=1.1.14
RUN curl -sSL https://install.python-poetry.org | python -
ENV PATH="/root/.local/bin:$PATH"

COPY pyproject.toml ./
COPY poetry.lock ./
RUN poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi

COPY *.py ./

ENTRYPOINT ["poetry", "run", "python", "-m"]
CMD []
